Faris Ajeddig - Jean-Baptiste Skutnik

# Modélisation d'un oscillateur harmonique

## Summary

During this 6-week long project, solutions to the 1D quantum harmonic oscillator will be calculated and some of their properties will be checked.

## Procedure d'install sur Ubuntu

sudo apt-get install cxxtest
sudo apt-get install gnuplot
sudo apt install doxygen
