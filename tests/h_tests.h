#include <cxxtest/TestSuite.h>
#include "../src/H.h"
#include "test_data.h"

arma::vec z = arma::linspace<arma::vec>(-10, 10, 101);
arma::vec z_rand = arma::vec(100).randu();

class H_Tests : public CxxTest::TestSuite {
    public:
	void testConstruction(void) {
	    H sample = H(z_rand);
	    TS_ASSERT((sample.get_z() == z_rand).min() == 1);
	}

	void testHn(void) {
	    H sample = H(z_rand);
	    TS_ASSERT((sample.Hi(0) == arma::vec(z_rand.n_elem, arma::fill::ones)).min() == 1);
	    TS_ASSERT((sample.Hi(1) == z_rand*2).min() == 1);
	}


	void testValues(void) {
	    H sample = H(z);
	    TS_ASSERT(norm(sample.Hi(2) - h2) < 10e-5);
	    TS_ASSERT(norm(sample.Hi(5) - h5) < 10e-5);
	    TS_ASSERT(norm(sample.Hi(10) - h10) < 10e-2);
	}
};
