#include <cxxtest/TestSuite.h>
#include "../src/calc.h"
#include "test_data.h"

arma::vec z = arma::linspace<arma::vec>(-10, 10, 101);

class Calc_Tests : public CxxTest::TestSuite {
    public:
	void testPsy(void) {
	    arma::vec sample = psy(z, 3);
	}

	void testOrtho(void) {
	    int test_order = 13;
	    double facti;
	    double factj;
	    H sample = H(gh_xis);

	    for (int i = 0; i < test_order; i++) {
		facti = std::pow(std::pow(2, i)*fact(i), -0.5);
		for (int j = 0; j < test_order; j++) {
		    factj = std::pow(std::pow(2, j)*fact(j), -0.5);
		    if (delta(i, j)) {
			TS_ASSERT(accu(std::pow(arma::datum::pi, -0.5)*facti*factj*gh_wis%sample.Hi(i)%sample.Hi(j)) - 1 < 10e-5);
		    } else {
			TS_ASSERT(accu(std::pow(arma::datum::pi, -0.5)*facti*factj*gh_wis%sample.Hi(i)%sample.Hi(j)) < 10e-5);
		    }
		}
	    }
	}
};
