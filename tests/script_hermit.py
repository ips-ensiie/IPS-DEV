#!/usr/bin/python

import numpy

numpy.set_printoptions(precision=2)

#test = numpy.polynomial.hermite.Hermite([-30240, 0, 302400, 0, -403200, 0, 161280, 0, -23040, 0, 1024])
test = numpy.polynomial.hermite.Hermite([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
values = test.linspace(101, [-10, 10])

index = 0
print('{')
for k in range(len(values[0])):
    index+= 1
    print(values[1][k], ', ', end='')
    if index%4 == 0:
        print('')
print('}')

