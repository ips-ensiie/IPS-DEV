/*! \file calc.h
 * \brief Prototypes of calculation functions
 *
 * Regroups all functions using project's objects to calculate a result. */

#ifndef CHARMONY_CALC_H
#define CHARMONY_CALC_H

#include<math.h>
#include "H.h"
#include "defines.h"

/*! \brief Calculate the values of psyn
 *
 * Will process `z` and return an `arma::vec` containing the values of `psyn(z)`.
 *
 * \param z The vector to use as reference on the x-axis
 * \param n The index of the values to calculate
 *
 * \return The calculated values. */
arma::vec psy(arma::vec z, int n);

long long fact(int);

#endif
