/*! \file H.h
 * \brief Header specifying the contents of H objects */

#ifndef CHARMONY_H_H
#define CHARMONY_H_H

#include <iostream>
#include <armadillo>

/*  _   _
 * | | | |
 * | |_| |
 * |  _  |
 * |_| |_|
 */

/*! \class H
 * \brief Class allowing to generate Hermite polynomials with ease
 *
 * When instanciated, it is bound to a set of coordinates passed as an argument. It will keep track of generated results to save time when using it in batches. */
class H
{
    public:
	/*! \brief Constructor
	 *
	 * \param z The set of coordinates to use */
	H(arma::vec z);

	/*! \brief Get Hn(z)
	 *
	 * Returns a vector with the values of Hn(z)
	 *
	 * \param n The index of the H to query */
	arma::vec Hi(int n);

	/*! \brief Returns the vector on which the H was defined
	 *
	 * \return The original vector
	 */
	arma::vec get_z();

    private:
	/*! \brief The history of calculated `Hi`s
	 *
	 * Contains all the calculated columns. `Hall.col(i)` contains Hi(z) */
	arma::mat Hall;

	/*! \brief The z used to generate H */
	arma::vec z;

	/*! \brief Calculate the missing columns to get the values of Hn
	 *
	 * Will populate `Hall` with every column from the last calculated to the queried column.
	 * Upon success, the function will return the queried column.
	 *
	 * \param n The index of the column to calculate.
	 * \return The queried column
	 * */
	arma::vec calcul(int n);
};

#endif
