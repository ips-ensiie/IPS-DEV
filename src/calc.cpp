/*! \file calc.cpp
 * \brief Definition of functions prototyped `in calc.h` */

#include "calc.h"

//Calcule la factorielle de valeur
long long fact (int valeur) {
    long long resultatFact = 1;
    for(int i = 1; i <= valeur; i++) {
	resultatFact *= i;
    }
    return resultatFact;
}

arma::vec psy(arma::vec z, int n) {
    H hBuffer = H(std::sqrt(MASS*OMEGA/PLANK_CONSTANT)*z);
    long double fact1 = std::pow(std::pow(2, n)*fact(n), -0.5);
    long double fact2 = std::pow(MASS*OMEGA/(arma::datum::pi*PLANK_CONSTANT), .25);
    arma::vec fact3 = exp(-MASS*OMEGA*z%z/(2*PLANK_CONSTANT));
    return fact1*fact2*fact3%hBuffer.Hi(n);
}
