/*! \file output.h
 * \brief Prototypes of output related functions */

#ifndef CHARMONY_OUTPUT_H
#define CHARMONY_OUTPUT_H

#include<iostream>
#include<armadillo>

/*! \brief Prepare data to be plotted
 *
 * Will prettify data in order for it to be understood by third-party plotting software
 *
 * \param x The coordinates on the x-axis
 * \param y The coordinates on the y-axis
 */
void plot(arma::vec x, arma::vec y);

#endif
