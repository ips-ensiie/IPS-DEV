/*! \file main.cpp
 * \brief Program entry point */

#include "calc.h"
#include "output.h"

int main(int argc, char** argv){
    int n = 1;

    if (argc > 1)
	n = atoi(argv[1]);

    arma::vec z = arma::linspace<arma::vec>(-10, 10, 801);

    plot(z, psy(z, n));
    return 0;
}
