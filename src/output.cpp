/*! \file output.cpp
 * \brief Definition of output related functions */

#include "output.h"

void plot(arma::vec x, arma::vec y) {
    if (x.n_elem != y.n_elem)
	return;
    for (unsigned long long i = 0; i < x.n_elem; i++)
	std::cout << x[i] << " " << y[i] << "\n";
}
