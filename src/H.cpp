/*! \file H.cpp
 * \brief Definition of H objects methods */

#include "H.h"

H::H(arma::vec z)
{
    this->z = arma::vec(z);
    Hall = arma::mat (arma::vec(size(z), arma::fill::ones));
    Hall.insert_cols(1,2*z);
}

arma::vec H::calcul(int n)
{
    for (int i = Hall.n_cols; i<=n; i++)
	Hall.insert_cols(i, 2*z%Hall.col(i-1) - 2*(i-1)*Hall.col(i-2));

    return Hall.col(n);
}

arma::vec H::Hi(int i) {
    if ((unsigned long long) i < Hall.n_cols)
	return Hall.col(i);
    else
	return calcul(i);
}

arma::vec H::get_z() {
    return this->z;
}
