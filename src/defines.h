/*! \file defines.h
 * \brief Collection of constants definitions used throughout the project */

/*! \brief Definition of the plank constant's value */
#define PLANK_CONSTANT 1

/*! \brief Definition of the frequency's value */
#define OMEGA 1

/*! \brief Definition of the electron's mass value */
#define MASS 1
