CC=g++
CFLAGS=-Wall -std=c++11
LDFLAGS=-larmadillo
BIN_DIR=bin
SRC_DIR=src
DOC_DIR=doc
TST_DIR=tests

SOURCES=src/main.cpp $(BIN_DIR)/H.o $(BIN_DIR)/calc.o $(BIN_DIR)/output.o
TESTS=test_h test_calc
EXECUTABLE=$(BIN_DIR)/charmony

# Sort l'executable.
default: $(SOURCES)
	$(CC) $(SOURCES) $(CFLAGS) $(LDFLAGS) -o $(EXECUTABLE)
# Permet de séparer les sources et les fichiers binaires dans deux fichiers séparés
$(BIN_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) -c -o $@ $<
# Va génerer un fichier de test à partir des headers de tests.
$(SRC_DIR)/%.cpp: $(TST_DIR)/%.h
	cxxtestgen --error-printer -o $@ $<
# Lance les tests
tests: $(TESTS)

test_h: $(BIN_DIR)/H.o $(BIN_DIR)/h_tests.o
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/h_tests $(LDFLAGS)

test_calc: $(BIN_DIR)/H.o $(BIN_DIR)/calc.o $(BIN_DIR)/calc_tests.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $(BIN_DIR)/calc_tests
# Génére la documentation
dox:
	doxygen doc/Doxyfile
# Efface tout ce qui est compilé
clean:
	rm -f $(BIN_DIR)/*.o $(EXECUTABLE)
# Efface la doc en plus de ce qui a été compilé.
purge: clean
	rm -rf $(DOC_DIR)/html $(DOC_DIR)/latex $(BIN_DIR)/*
# Sort le graphe.
plot:
	./bin/charmony 11 | gnuplot -e "plot '-' using 1:2" -p
